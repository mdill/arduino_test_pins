byte fadePins[] = { 3, 5, 6, 9, 10, 11 };
byte fadeCount = 6;

byte brightness = 0;
byte fadeAmount = 5;

byte digitalPins[] = { 0, 1, 2, 4, 7, 8, 12, 13, 14, 15, 16, 17, 18, 19 };
byte digitalCount = 14;

void setup(){
  for( int i = 0; i < fadeCount; i++ )
    pinMode( fadePins[i], OUTPUT );

  for( int i = 0; i < digitalCount; i++ ){
    pinMode( digitalPins[i], OUTPUT );
    digitalWrite( digitalPins[i], HIGH );
  }
}

void loop(){
  // Set the brightness of analog pins
  for( int i = 0; i < fadeCount; i++ )
    analogWrite( fadePins[i], brightness );

  // Change the brightness for next time through the loop
  brightness = brightness + fadeAmount;

  // Reverse the direction of the fading at the ends of the fade
  if( brightness <= 0 || brightness >= 255 ){
    fadeAmount = -fadeAmount;
  }
  
  // Wait for 30 milliseconds to see the dimming effect
  delay(30);
}
