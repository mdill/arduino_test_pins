# Arduino Test Pins

## Purpose

This simple sketch is designed to turn all pins on an Arduino on, and fade with
PWM (if possible), to test if the pin is working, and if the solder joint is
making good contact.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/arduino_test_pins.git

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/arduino_test_pins/src/master/LICENSE.txt) file for
details.

